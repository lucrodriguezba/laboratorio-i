
import java.util.Arrays;
import java.util.Collections;

public class Elecciones {
	
	public static void main(String[] args){
		java.util.Scanner input = new java.util.Scanner(System.in);
		System.out.println("Bienbenido al sistema de gesti�n electoral "+
		"del departamento de Topaiti.\n");
		boolean continuar=true;
		while(continuar){
			System.out.println("Ingrese el n�mero de candidatos a registrar.");
			int numC = input.nextInt();
			System.out.println("Ingrese el n�mero de municipios a registrar.");
			int numM = input.nextInt();
			int dim;
			if(numM>numC){
				dim=numM;
			}else{
				dim=numC; 
			}
			if(dim<=20){
				String [][] candMun;
				candMun=IngresoDatos(dim,numM,numC);
				int[][] votos;
				votos=Votos(numC,numM,candMun);
				System.out.println("Ha ingresado los datos exitosamente.");
				boolean continuar1=true;
				while (continuar1){
					System.out.println("Seleccione el tipo de reporte que desea.");
					System.out.println("\t 1. Tabla.");
					System.out.println("\t 2. Porcentual (ganador).");
					System.out.println("\t 3. Orden (candidato m�s votado/segunda vuelta).");
					System.out.println("\t 4. Finalizar sesi�n.");
					int x=input.nextInt();
					switch (x){
						case 1:
							ReporteTabla(candMun,votos,numC,numM);
							break;
						case 2:
							ReportePorcentual(candMun,votos,numC,numM);
							break;
						case 3:
							ReporteOrden(candMun,votos,numC,numM);
							break;
						case 4:
							continuar=false;
							continuar1=false;
							break;
						default:
							System.out.println("Ingrese una opci�n v�lida.");
					}// fin switch reporte
				}//fin while reportes
			}else{
				System.out.println("Error: el sistema no admite m�s "+
				"de 20 municipos y 20 candidatos. Ingrese una opcion v�lida.\n");
			}//fin if para dimensi�n de matrices
		}//fin while 
		System.out.print("\nSesi�n Finalizada");
	}
	
	public static String[][] IngresoDatos(int dim,int numM,int numC){
		java.util.Scanner input = new java.util.Scanner(System.in);
		String [][] candMun= new String [dim][2];
		for(int k=0;k<numC;k++){
			System.out.println("Ingrese el candidato "+(k+1));				
			candMun[k][0]=input.next();
			}
		for(int i=0;i<numM;i++){
			System.out.println("Ingrese el municipio "+(i+1));				
			candMun[i][1]=input.next();
		}
		return candMun;	
	}//fin RegistroDatos
	
	public static int[][] Votos(int numC,int numM, String[][] candMun){
		java.util.Scanner input = new java.util.Scanner(System.in);
		int[][] votos= new int[numM][numC];
		for(int i=0;i<numM;i++){
			for(int k=0;k<numC;k++){
				System.out.println("Ingrese la cantidad de votos del candidato "+
				candMun[k][0]+" en el municipio "+ candMun[i][1]);
				votos[i][k]=input.nextInt();
			}
		}
		return votos;
	}//fin Votos
	
	public static void ReporteTabla(String[][] candMun,int[][] votos,int numC, int numM){
		for(int i=0;i<=numC;i++){
			switch (i){
				case 0:
					System.out.print("\t\t");
					break;
				default:
					System.out.print("\t"+candMun[(i-1)][0]+"\t");
			}//fin switch
		}//fin fila candidatos
		System.out.println("");
		for(int i=0;i<numM;i++){
			for (int k=0;k<=numC;k++){
				switch (k){
					case 0:
						System.out.print("\t"+candMun[i][1]+"\t");
						break;
					default:
						System.out.print("\t"+votos[i][(k-1)]+"\t");
				}//fin switch
			}//fin fila
			System.out.println("");
		}//fin filas votos
	}//fin ReporteTabla

	public static void ReportePorcentual(String[][] candMun,int[][] votos, int numC,int numM){
		//calculo del n�mero total de votos
		int totalVotos=0;
		for(int i=0;i<numM;i++){
			for(int k=0;k<numC;k++){
				totalVotos+=votos[i][k];
			}
		}//fin for total votos
		//calcular e imprimir porcentaje de votos por candidato	
		for (int k=0;k<numC;k++){
			int sum=0;
			for (int i=0;i<numM;i++){
				sum+=votos[i][k];
			}
			float porcentaje=(((float)sum/(float)totalVotos)*100);
			System.out.print("Candidato "+candMun[k][0]+" :");
			System.out.printf("%.2f",porcentaje);
			System.out.print("%");
			if(porcentaje>=50){
				System.out.println(" - Ganador.");
			}else{
				System.out.println("");
			}
		}
	}
	
	public static void ReporteOrden(String[][] candMun,int[][] votos,int numC,int numM){
		Integer[] votosPorCandidato= new Integer [numC];
		for(int i=0;i<numC;i++){
			int sum=0;
			for(int k=0;k<numM;k++){
				sum+=votos[k][i];
			}
			votosPorCandidato[i]=sum;
		}//crear arreglo con la cantidad total de votos correspondiente a cada candidato
		Integer[] tmp=votosPorCandidato.clone();
		Arrays.sort(tmp, Collections.reverseOrder());
		for (int i=0;i<tmp.length;i++){
			for (int k=0;k<votosPorCandidato.length;k++){
				int aux=votosPorCandidato[k];
				if(tmp[i]==votosPorCandidato[k]){
					if(i<2){
						System.out.println((i+1)+" "+candMun[k][0]+" "+tmp[i]+" Va a segunda vuelta");
					}else{
						System.out.println((i+1)+" "+candMun[k][0]+" "+tmp[i]);
					}
				}
			}
		}
	}
}