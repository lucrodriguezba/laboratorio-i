
import java.util.Arrays;

public class LocalComercial {

	public static void main(String[] args) {
		java.util.Scanner input= new java.util.Scanner(System.in);
		boolean primerIngreso=true;
		String[][] datosProductos=new String[20][3];	
		int[][] datosVentas=new int[50][11];
		String[][] datosVendedores=new String[20][3];
		int[][] cProm=new int[5][2];
		boolean continuar=true;
		while (continuar){
			if (primerIngreso){
				System.out.println("Por favor ingrese los datos de sus productos, sus vendedores y sus ventas.");
				datosProductos=DatosProductos(datosProductos,primerIngreso);
				datosVendedores=DatosVendedores(datosVendedores,primerIngreso);
				cProm=CodigosPromocionales(cProm);
				datosVentas=DatosVentas(datosVentas,primerIngreso,cProm);
				primerIngreso=false;
			}else{
				System.out.println("¿Qué operación desea realizar?");
				System.out.println(" 1. Agregar productos.");
				System.out.println(" 2. Agregar vendedores.");
				System.out.println(" 3. Agregar venta.");
				System.out.println(" 4. Ver vendedor con más ventas.");
				System.out.println(" 5. Ver ventas con tarjeta debito");
				System.out.println(" 6. Buscar una venta.");
				System.out.println(" 7. Ver el total de ingresos de las ventas.");
				System.out.println(" 8. Generar factura de cada venta.");
				System.out.println(" 9. Ver la venta de mayor importe realizada con tarjeta.");
				System.out.println(" 10. Finalizar sesión.");
			int elección = input.nextInt();
			switch(elección){
				case 1:
					datosProductos=DatosProductos(datosProductos,primerIngreso);
					break;
				case 2:
					datosVendedores=DatosVendedores(datosVendedores,primerIngreso);
					break;
				case 3:
					datosVentas=DatosVentas(datosVentas,primerIngreso, cProm);
					break;
				case 4:
					VendedorEstrella(datosVentas,datosVendedores);
					break;
				case 5:
					VentasDebito(datosVentas, datosVendedores, cProm);
					break;
				case 6:
					BuscadorVentas(datosVentas, datosVendedores, datosProductos, cProm);
					break;
				case 7:
					IngresosTotales(datosVentas,cProm);
					break;
				case 8:
					Facturas(datosVentas, datosVendedores, datosProductos, cProm);
					break;
				case 9:
					VentaCredito(datosVentas, cProm);
					break;
				case 10:
					System.out.println("Sesión fializada, que tenga un buen día.");
					continuar=false;
					break;
				default:
					System.out.println("Ingrese una opción válida.\n\n");	
			}		
			}
		}
	}
	
	public static String[][] DatosProductos(String[][] datosProductos, boolean primerIngreso){
		java.util.Scanner input= new java.util.Scanner(System.in);
		while (true){
			System.out.println("Ingrese el número de productos que va a ingresar");
			int cantP=input.nextInt();
			if(primerIngreso){
				if(cantP<=20){
					datosProductos=new String[20][3];
					for (int i=0; i<cantP;i++){
						System.out.println("Ingrese el codigo del producto "+(i+1));
						datosProductos[i][0]=input.next();
						System.out.println("Ingrese el nombre del producto "+(i+1));
						datosProductos[i][1]=input.next();
						System.out.println("Ingrese el precio unitario del producto "+(i+1));
						datosProductos[i][2]=input.next();
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor válido. Puede ingresar hasta 20 productos.");
				}
			}else{
				int x=0;
				for (int i=0;i<20;i++){
					if(datosProductos[i][0]==null){
						x=i;
						break;
					}
				}
				if((x+cantP)<=20){
					for(int k=x;k<(cantP+x);k++){
						System.out.println("Ingrese el codigo del producto "+(k+1));
						datosProductos[k][0]=input.next();
						System.out.println("Ingrese el nombre del producto "+(k+1));
						datosProductos[k][1]=input.next();
						System.out.println("Ingrese el precio unitario del producto "+(k+1));
						datosProductos[k][2]=input.next();
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor válido. Puede ingresar  "
					+(20-x)+" productos más.");
				}
			}
		}	
		return datosProductos;
	}
	
	public static int[][] DatosVentas(int[][] datosVentas, boolean primerIngreso, int[][] cProm){
		java.util.Scanner input= new java.util.Scanner(System.in);
		while(true){
			System.out.println("Ingrese el número ventas a ingresar.");
			int cantV=input.nextInt();
			if(primerIngreso){
				if (cantV<=50){
					for (int i=0; i<cantV;i++){
						System.out.println("Ingrese el día de la venta "+(i+1));
						datosVentas[i][0]=input.nextInt();
						System.out.println("Ingrese el mes de la venta "+(i+1));
						datosVentas[i][1]=input.nextInt();
						System.out.println("Ingrese el año de la venta "+(i+1));
						datosVentas[i][2]=input.nextInt();
						System.out.println("Ingrese la hora de la venta (sólo en horas) "+(i+1));
						datosVentas[i][3]=input.nextInt();
						System.out.println("Ingrese los minutos de la hora de la venta "+(i+1));
						datosVentas[i][4]=input.nextInt();
						System.out.println("Ingrese el ID del vendedor de la venta "+(i+1));
						datosVentas[i][5]=input.nextInt();
						System.out.println("Ingrese el codigo del producto vendido "+(i+1));
						datosVentas[i][6]=input.nextInt();
						System.out.println("Ingrese el precio unitario del producto vendido "+(i+1));
						datosVentas[i][7]=input.nextInt();
						System.out.println("Ingrese la cantidad de producto vendida "+(i+1));
						datosVentas[i][8]=input.nextInt();
						System.out.println("Ingrese la forma de pago (0 - efectivo, 1 - debito, 2 - tarjeta) "+(i+1));
						datosVentas[i][9]=input.nextInt();
						System.out.println("Ingrese el codigo promocional de la venta (si no tiene ingrese 0) "+(i+1));
						datosVentas[i][10]=input.nextInt();
						
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor válido. Puede ingresar hasta 50 ventas.");
				}
			}else{
				int x=0;
				for (int i=0;i<50;i++){
					if(datosVentas[i][0]==0){
						x=i;
						break;
					}
				}
				if((cantV+x)<=50){
					for (int k=x;k<=(cantV+x);k++){
						System.out.println("Ingrese el día de la venta "+(k+1));
						datosVentas[k][0]=input.nextInt();
						System.out.println("Ingrese el mes de la venta "+(k+1));
						datosVentas[k][1]=input.nextInt();
						System.out.println("Ingrese el año de la venta "+(k+1));
						datosVentas[k][2]=input.nextInt();
						System.out.println("Ingrese la hora de la venta (sólo en horas) "+(k+1));
						datosVentas[k][3]=input.nextInt();
						System.out.println("Ingrese los minutos de la hora de la venta "+(k+1));
						datosVentas[k][4]=input.nextInt();
						System.out.println("Ingrese el ID del vendedor "+(k+1));
						datosVentas[k][5]=input.nextInt();
						System.out.println("Ingrese el codigo del producto vendido"+(k+1));
						datosVentas[k][6]=input.nextInt();
						System.out.println("Ingrese el precio unitario del producto "+(k+1));
						datosVentas[k][7]=input.nextInt();
						System.out.println("Ingrese la cantidad que se vendió "+(k+1));
						datosVentas[k][8]=input.nextInt();
						System.out.println("Ingrese la forma de pago (0 - efectivo, 1 - debito, 2 - tarjeta) "+(k+1));
						datosVentas[k][9]=input.nextInt();
						System.out.println("Ingrese el codigo promocional de la venta (si no tiene ingrese 0) "+(k+1));
						datosVentas[k][10]=input.nextInt();
					}
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor válido. Puede ingresar  "
					+(50-x)+" productos más.");
				}
			}
		}
		return datosVentas;
	}
	
	public static String[][] DatosVendedores(String[][]datosVendedores,boolean primerIngreso){
		java.util.Scanner lectura=new java.util.Scanner(System.in);
		System.out.println("Ingrese el número de vendedores:");
		int numV=lectura.nextInt();
		while(true){
			if (primerIngreso){
				if (numV<=20){
					datosVendedores=new String [20][3];
					for (int i=0;i<numV;i++){
						System.out.println("Ingrese el ID del vendedor "+(i+1)+":");
						datosVendedores[i][0]=lectura.next();
						System.out.println("Ingrese el Nombre del vendedor "+(i+1)+":");
						datosVendedores[i][1]=lectura.next();
						System.out.println("Ingrese el Apellido del vendedor "+(i+1)+":");
						datosVendedores[i][2]=lectura.next();
					}
					break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor válido. Puede ingresar hasta 20 productos.");
				}
			}else{
				int x=0;
				for (int i=0;i<20;i++){
					if (datosVendedores[i][0]==null){
						x=i;
						break;
					}
				}
				if((numV+x)<=20){
					for (int i=x;i<(numV+x);i++){
						System.out.println("Ingrese el ID del vendedor "+(i+1)+":");
						datosVendedores[i][0]=lectura.next();
						System.out.println("Ingrese el Nombre del vendedor "+(i+1)+":");
						datosVendedores[i][1]=lectura.next();
						System.out.println("Ingrese el Apellido del vendedor "+(i+1)+":");
						datosVendedores[i][2]=lectura.next();
					}
				break;
				}else{
					System.out.println("No hay espacio en memoria, ingrese un valor válido. Puede ingresar "+(20-x)+" productos más.");
				}
			}
		}
		return datosVendedores;
	}
	
	public static int[][] CodigosPromocionales(int[][] cProm){
		java.util.Scanner input=new java.util.Scanner(System.in);
		System.out.println("Ingrese el número de códigos promocionales, puede ingresar hasta 5 codigos.");
		int numC=input.nextInt();
		for (int i=0;i<numC;i++){
			System.out.println("Ingrese el codigo "+(i+1));
			cProm[i][0]=input.nextInt();
			System.out.println("Ingrese el valor del descuento para el codigo "+(i+1)+" (ej: 200 o 500)");
			cProm[i][1]=input.nextInt();
		}
		return cProm;
	}
	
	public static void VendedorEstrella(int[][] datosVentas,String[][] datosVendedores){
		int x=0;
		for (int i=0;i<20;i++){
			if (datosVendedores[i][0]==null){
				x=i;
				break;
			}
		}
		int y=0;
		for (int i=0;i<50;i++){
			if (datosVentas[i][0]==0){
				y=i;
				break;
			}
		}
		int c=0;
		int d=0;
		int b=0;
		int sum=0;
		for(int j=0;j<x;j++){
			for(int i=0;i<y;i++){
				b=Integer.parseInt(datosVendedores[j][0]);
				if (datosVentas[i][5]==b){
					sum=0;
					sum++;
				}
			}
			if (sum>c){
				c=sum;
				d=j;
			}
		}
		System.out.println("El vendedor con más venta tiene la siguiente información:");
		System.out.println(" \t ID \t Nombre \t Apellido");
		System.out.println("\t"+datosVendedores[d][0]+" \t "+datosVendedores[d][1]+" \t \t "+datosVendedores[d][2]);
	}
	
	public static void VentasDebito(int[][] datosVentas, String[][] datosVendedores, int[][] cProm){	
		int y=0;
		for (int i=0;i<20;i++){
			if(datosVendedores[i][0]==null){
				y=i;
				break;
			}
		}
		int[] vID=new int[y];
		for (int i=0;i<y;i++){
			vID[i]=Integer.parseInt(datosVendedores[i][0]);
		}
		int cantVnts=0;
		for (int i=0;i<50;i++){
			if(datosVentas[i][0]==0){
				cantVnts=i;
				break;
			}
		}
		Arrays.sort(vID);
		for (int i=0;i<y;i++){
			for (int k=0;k<cantVnts;k++){
				if(vID[i]==datosVentas[k][5] && datosVentas[k][9]==1){
					System.out.println("\t Hora \t Fecha \t \t ID "+" \t Codigo \t Precio \t Cantidad \t Codigo Promocional \t Total \t");
					System.out.print("\t "+datosVentas[k][3]+":"+datosVentas[k][4]+ " \t "+datosVentas[k][0]+
						"/"+datosVentas[k][1]+"/"+datosVentas[k][2]+" \t "+datosVentas[k][5]+" \t "+datosVentas[k][6]+
						" \t \t "+datosVentas[k][7]+" \t \t "+datosVentas[k][8]+" \t \t  "+datosVentas[k][10]+" \t \t \t");
					int total=datosVentas[k][7]*datosVentas[k][8];
					for (int q=0;q<5;q++){
						if(datosVentas[k][10]==cProm[q][0] && datosVentas[k][10]!=0){
							System.out.println((total-cProm[q][1]));
							break;
						}else{
							if(q==4){
								System.out.println(total);
							}
							
						}
					}
				}
			}
		}
	}

	public static void BuscadorVentas(int[][] datosVentas, String[][] datosVendedores, String[][]datosProductos, int[][] cProm){
		java.util.Scanner input=new java.util.Scanner(System.in);
		int x=0;
		for (int i=0;i<20;i++){
			if(datosProductos[i][0]==null){
				x=i;
				break;
			}
		}
		int y=0;
		for (int i=0;i<20;i++){
			if(datosVendedores[i][0]==null){
				y=i;
				break;
			}
		}
		boolean continuar=true;
		while (continuar){
			for (int j=0;j<x;j++){
				System.out.println("\t"+datosProductos[j][0]+"\t"+datosProductos[j][1]+"\t"+datosProductos[j][2]);
			}
			System.out.println("Ingrese el codigo del producto que desea consultar.");
			int elecciónProducto=input.nextInt();
			for (int j=0;j<y;j++){
				System.out.println("\t"+datosVendedores[j][0]+"\t"+datosVendedores[j][1]+"\t"+datosVendedores[j][2]);
			}
			System.out.println("Ingrese el codigo del vendedor que desea consultar.");
			int elecciónVendedor=input.nextInt();
			int cantVnts=0;
			for (int i=0;i<50;i++){
				if(datosVentas[i][0]==0){
					cantVnts=i;
					break;
				}
			}
			for(int i=0;i<cantVnts;i++){
				if(elecciónProducto==datosVentas[i][6] && elecciónVendedor==datosVentas[i][5]){
					System.out.println("Los resultados de su búsqueda son:");
					System.out.println("\t Hora \t Fecha \t \t ID "+" \t Codigo \t Precio \t Cantidad \t Total \t Forma de pago");
					System.out.print("\t "+datosVentas[i][3]+":"+datosVentas[i][4]+ " \t "+datosVentas[i][0]+
						"/"+datosVentas[i][1]+"/"+datosVentas[i][2]+" \t "+datosVentas[i][5]+" \t "+datosVentas[i][6]+
						" \t \t "+datosVentas[i][7]+" \t \t "+datosVentas[i][8]+" \t \t  ");
					int total=datosVentas[i][7]*datosVentas[i][8];
					for (int q=0;q<5;q++){
						if(datosVentas[i][10]==cProm[q][0] && datosVentas[i][10]!=0){
							System.out.print((total-cProm[q][1])+" \t ");
							break;
						}else{
							if(q==4){
								System.out.print(total+" \t ");
							}
						}
					}
					switch(datosVentas[i][9]){
					case 0:
						System.out.println("Efectivo");
						break;
					case 1:
						System.out.println("Debito");
						break;
					case 2:
						System.out.println("Tarjeta");
						break;
					}
				}else{
					if(i==(cantVnts-1)  ){
						System.out.println("No se encontraron resultados");
					}
				}
			}
		System.out.println("¿Desea hacer otra búsqueda? (1 - Si, 0 - No)");
		int yn=input.nextInt();
		if(yn==0){
			continuar=false;
		}
		}
	}
	
	public static void Facturas(int[][] datosVentas, String[][] datosVendedores, String[][] datosProductos, int[][] cProm){
		java.util.Scanner input=new java.util.Scanner(System.in);
		int cantVnts=0;
		int cantP=0;
		int cantV=0;
		for (int i=0;i<50;i++){
			if(datosVentas[i][0]==0){
				cantVnts=i;
				break;
			}
		}
		for (int i=0;i<20;i++){
			if(datosVendedores[i][0]==null){
				cantV=i;
				break;
			}
		}
		int[] vendedoresID=new int[(cantV+1)];
		for (int k=0;k<(vendedoresID.length-1);k++){
			vendedoresID[k]=Integer.parseInt(datosVendedores[k][0]);
		}
		for (int i=0;i<20;i++){
			if(datosProductos[i][0]==null){
				cantP=i;
				break;
			}
		}
		int[] productosCodigo=new int[cantP+1];
		for (int k=0;k<(productosCodigo.length-1);k++){
			productosCodigo[k]=Integer.parseInt(datosProductos[k][0]);
		}
		for(int i=0;i<cantVnts;i++){
			System.out.println("**************************************************************************"
					+ "***************************\n");
			System.out.println("Fecha y hora de la venta: "+datosVentas[i][0]+"/"+datosVentas[i][1]+"/"+datosVentas[i][2]+
				"\t"+datosVentas[i][3]+":"+datosVentas[i][4]+"\n");
			System.out.println(	);
			System.out.println("\t ID \t Nombre \t Apellido");
			//relacionar arreglo ID para imprimir nobre y apellido
			for(int k=0;k<vendedoresID.length;k++){
				if(datosVentas[i][5]==vendedoresID[k]){
					System.out.println("\t "+datosVendedores[k][0]+"\t "+datosVendedores[k][1]+"\t\t "+datosVendedores[k][2]);
				}
			}
			System.out.println("Datos de la venta: ");
			System.out.println("\t Codigo    Nombre       Precio unitario     Cantidad     Subtotal venta     Forma de pago");
			//relacionar arreglo codigo para imprimir datos de la venta
			for (int h=0;h<productosCodigo.length;h++){
				if(datosVentas[i][6]==productosCodigo[h]){
					System.out.print(" \t "+datosVentas[i][6]+" \t   "+datosProductos[h][1]+" \t "+datosVentas[i][7]+
							" \t\t\t"+datosVentas[i][8]+"\t\t");
					int total=datosVentas[i][7]*datosVentas[i][8];
					for (int q=0;q<5;q++){
						if(datosVentas[i][10]==cProm[q][0] && datosVentas[i][10]!=0){
							System.out.print((total-cProm[q][1])+" \t\t ");
							break;
						}else{
							if(q==4){
								System.out.print(total+" \t\t ");
							}
						}
					}
					switch(datosVentas[i][9]){
					case 0:
						System.out.println("Efectivo");
						break;
					case 1:
						System.out.println("Debito");
						break;
					case 2:
						System.out.println("Tarjeta");
						break;
					}
				}
			}
			System.out.println("**************************************************************************"
					+ "***************************\n");
		}	
	}

	public static void IngresosTotales(int[][]datosVentas, int[][] cProm ){
		int cantVnts=0;
		for (int i=0;i<50;i++){
			if(datosVentas[i][0]==0){
				cantVnts=i;
				break;
			}
		}
		int sum=0;
		for (int k=0;k<cantVnts;k++){
			for(int q=0;q<5;q++){
				if (datosVentas[k][10]==cProm[q][0]){
					sum+=(datosVentas[k][7]*datosVentas[k][8])-cProm[q][1];
					break;
				}
				
			}
		}
		System.out.println("El monto total de ventas es $"+sum);
	}
	
	public static void VentaCredito(int[][]datosVentas, int[][] cProm){
		int x=0;
		for (int i=0;i<50;i++){
			if (datosVentas[i][0]==0){
				x=i;
				break;
			}
		}
		int b;
		int sum=0;
		int c=0;
		int d=0;
		for (int i=0;i<x;i++){
			if (datosVentas[i][9]==2){
				sum+=datosVentas[i][7]*datosVentas[i][8];
			}
			if (sum>c){
				c=sum;
				d=i;
			}
		}
		System.out.println("\t Fecha \t\t Hora \t ID vendedor \t Codigo producto \t Precio unitario \t Cantidad \t Total");
		System.out.print("\t"+datosVentas[d][0]+"/"+datosVentas[d][1]+"/"+datosVentas[d][2]+" \t "+
				datosVentas[d][3]+":"+datosVentas[d][4]+" \t "+datosVentas[d][5]+" \t \t "+datosVentas[d][6]+" \t \t \t "+
				datosVentas[d][7]+" \t \t \t "+datosVentas[d][8]+" \t \t ");
		int total=datosVentas[d][7]*datosVentas[d][8];
		for (int q=0;q<5;q++){
			if(datosVentas[d][10]==cProm[q][0] && datosVentas[d][10]!=0){
				System.out.println((total-cProm[q][1]));
				break;
			}else{
				if(q==4){
					System.out.println(total);
				}
			}
		}
	}

}

