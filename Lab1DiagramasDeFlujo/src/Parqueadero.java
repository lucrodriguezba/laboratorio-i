public class Parqueadero {

	public static void main(String[] args) {
		java.util.Scanner lectura=new java.util.Scanner(System.in);
		System.out.println("Bienvenido al programa.");
		System.out.println("Ingrese la tarifa por hora a cobrar:");
		int tarifa=lectura.nextInt();
		int a=0;
		int b=0;
		int c=0;
		int h=0;
		String consola[][]=new String [3][29];
		int horaEntrada[][]=new int [3][29];
		int puestosOcupados[]=new int [87];
		int salidasGuardadas[]=new int[87];
		for (int i=0;i<3;i++){
			for(int j=0;j<29;j++){
				a=b+j;
				if (j<9 && i<1){
					consola[i][j]="0"+String.valueOf(a+1);
				}else{
					consola[i][j]=String.valueOf(a+1);
				}
			}
			b=a+1;
		}
		boolean d=true;
		while(d==true){
			System.out.println("żQué operación desea realizar?");
			System.out.println("1. Dar entrada a un nuevo auto.");
			System.out.println("2. Dar salida a un auto.");
			System.out.println("3. Ver los ingresos del día.");
			System.out.println("4. Mostar los puestos vacíos del parquedero.");
			System.out.println("5. Finalizar programa.");
			int eleccion=lectura.nextInt();
			switch(eleccion){
				case 1:
					Entrada(consola,horaEntrada);
					break;
				case 2:
					salidasGuardadas[h]=Salida(consola,puestosOcupados,horaEntrada,tarifa);
					System.out.println("El valor a pagar es: $"+salidasGuardadas[h]);
					h+=1;
					c+=1;
					break;
				case 3:
					IngresosParciales(salidasGuardadas,c);
					break;
				case 4:
					ImpresionConsola(consola);
					break;
				case 5:
					d=false;
					break;
			}
		}
	}
	public static void Entrada(String consola[][],int horaEntrada[][]){
		java.util.Scanner lectura=new java.util.Scanner(System.in);
		System.out.println("Introduzca la hora de entrada del auto que ingresa(6-20):");
		int horaE=lectura.nextInt();
		int a=0;
		int b=0;
		for (int i=0;i<3;i++){
			for (int j=0;j<29;j++){
				a=b+j;
				if (consola[i][j]!="**"){
					consola[i][j]="**";
					horaEntrada[i][j]=horaE;
					System.out.println("El puesto asigando es el número: "+(a+1));
					i=3;
					break;
				}
			}
			b=a+1;
		}
	}
	public static void ImpresionConsola(String consola[][]){
		for (int i=0;i<3;i++){
			for(int j=0;j<29;j++){
				if (j<28){
					System.out.print("|"+consola[i][j]);
				}else{
					System.out.print("|"+consola[i][j]+"|");
				}
			}
			System.out.println(" ");
		}
		System.out.println("-> Los dos asteriscos significan que está ocupado el lugar. El resto está vacío.");
		int g=0;
		for (int i=0;i<3;i++){
			for (int j=0;j<29;j++){
				if (consola[i][j]!="**"){
					g+=1;
				}
			}
		}
		System.out.println("Quedan "+g+" puestos libres.");
	}
	public static int Salida(String consola[][],int puestosOcupados[],int horaEntrada[][],int tarifa){
		int a=0;
		int b=0;
		int c=0;
		for (int i=0;i<3;i++){
			for (int j=0;j<29;j++){
				a=j+b;
				if (consola[i][j]=="**"){
					puestosOcupados[a]=a+1;
					c+=1;
				}else{
					puestosOcupados[a]=0;
				}
			}
			b=a+1;
		}
		int d=0;
		System.out.print("Ingrese el número del puesto del auto que va a salir(opciones posibles: ");
		for (int i=0;i<87;i++){
			if (puestosOcupados[i]!=0){
				d+=1;
				if (d<c){
					System.out.print(puestosOcupados[i]+", ");
				}else{
					System.out.print(puestosOcupados[i]+"):");
				}
			}
		}
		java.util.Scanner lectura=new java.util.Scanner(System.in);
		int autoS=lectura.nextInt();
		autoS-=1;
		if (autoS<9){
			consola[autoS/29][autoS-((autoS/29)*29)]="0"+String.valueOf(autoS+1);
		}else{
			consola[autoS/29][autoS-((autoS/29)*29)]=String.valueOf(autoS+1);
		}
		System.out.println("Ingrese la hora de salida(7-21):");
		int horaS=lectura.nextInt();
		int plataPagar=((horaS-horaEntrada[autoS/29][autoS-((autoS/29)*29)])*tarifa);
		return plataPagar;
	}
	public static void IngresosParciales(int salidasGuardadas[],int c){
		int ingresos=0;
		for (int i=0;i<c;i++){
			ingresos+=salidasGuardadas[i];
		}
		System.out.println("Los ingresos del día son: $"+ingresos);
	}
}
