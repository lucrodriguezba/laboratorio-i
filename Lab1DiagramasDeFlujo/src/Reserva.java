public class Reserva {
	public static void main(String[] args) {

		char[][] matrixec = new char[7][6];
		int[][] eccedula = new int[7][6];
		String[][] ecnombre = new String[7][6];

		char[][] matrixej = new char[2][4];
		int[][] ejcedula = new int[2][4];
		String[][] ejnombre = new String[2][4];

		int posventana = 0;
		int pospasillo = 0;
		int poscentro = 0;
		int j = 0;

		int posventanaej = 0;
		int pospasilloej = 0;
		int k = 0;

		int[] mecnecmejnej = new int[4];

		boolean continuar = true;
		while (continuar == true) {
				java.util.Scanner op = new java.util.Scanner(System.in);
				int opcion;
				System.out.println("Bienvenido a la pagina de reservas. Por favor, seleccione una opción");
				System.out.println("1. Reservar asientos" + "\n" + "2. Reportes" + "\n" + "3. Salir");
				opcion = op.nextInt();
				switch (opcion) {
				case 1:
						java.util.Scanner cl = new java.util.Scanner(System.in);
						int clase;
						System.out.println("Seleccione la clase en la que quiere reservar su asiento");
						System.out.println("1. Economica" + "\n" + "2. Ejecutiva");
						clase = cl.nextInt();
						switch (clase) {
						case 1: 
							System.out.println("A continuación se mostrará el mapa de los asientos en clase económica");
							asientosecprint(matrixec);
							int posicion;
							java.util.Scanner pos = new java.util.Scanner(System.in);
							System.out.println("Por favor, seleccione su posición");
							System.out.println("1. Ventana" + "\n" + "2. Centro" + "\n" + "3. Pasillo");
							posicion = pos.nextInt();
							switch (posicion){
							case 1: {while (j == 0 || j == 5) {
									ventanaec(eccedula, posventana);
									cedulaec(eccedula, posventana, poscentro, pospasillo, j);
									nombreec(ecnombre, posventana, pospasillo, poscentro, j);
									break;
								}};
							break;
							case 2: {while (j == 1 || j == 4) {
									centroec(eccedula, poscentro);
									cedulaec(eccedula, posventana, poscentro, pospasillo, j);
									nombreec(ecnombre, posventana, pospasillo, poscentro, j);
									break;
								}};
							break;
							case 3: {while (j == 2 || j == 3) {
									pasilloec(eccedula, pospasillo);
									cedulaec(eccedula, posventana, poscentro, pospasillo, j);
									nombreec(ecnombre, posventana, pospasillo, poscentro, j);
									break;
								}};
							
							break;
							default:System.out.println("Por favor seleccione una opcion valida");
									break;
							};
						
						break;
						case 2:
							System.out.println("A continuación se mostrará el mapa de los asientos en clase ejecutiva");
							asientosejprint(matrixej);
							int posicionej;
							java.util.Scanner posej = new java.util.Scanner(System.in);
							System.out.println("Por favor, seleccione su posición");
							System.out.println("1. Ventana" + "\n" + "2. Pasillo");
							posicionej = posej.nextInt();;
							switch (posicionej) {
							case 1:
								while (k == 0 || k == 3) {
									ventanaej(ejcedula, posventanaej);
									cedulaej(ejcedula, posventanaej, pospasilloej, k);
									nombreej(ejnombre, posventanaej, pospasilloej, k);
								}
							break;
							case 2:
								while (k == 1 || k == 2) {
									pasilloej(ejcedula, pospasilloej);
									cedulaej(ejcedula, posventanaej, pospasilloej, j);
									nombreej(ejnombre, posventanaej, pospasilloej, j);
								}
							break;
							default:
								System.out.println("Por favor seleccione una opcion valida");
							break;

							}
						break;
						}
					
				break;
				case 2:
					java.util.Scanner re = new java.util.Scanner(System.in);
					int reporte;
					System.out.println("1. Cancelar reservas" + "\n" + "2. Buscar asiento por cédula" + "\n"
							+ "3. Buscar si hay dos personas con el mismo nombre" + "\n"
							+ "4. Número de sillas ejecutivas ocupadas" + "\n" + "5. Sillas económicas disponibles"
							+ "\n" + "6. Sillas económicas disponibles en la ventana" + "\n"
							+ "7. Mapa de sillas del avión");
					reporte = re.nextInt();
					switch (reporte) {
					case 1:
						System.out.println("Elija la clase en la que se reservó su asiento");
						java.util.Scanner ca = new java.util.Scanner(System.in);
						int claseavion;
						System.out.println("1. Economica" + "\n" + "2. Ejecutiva");
						claseavion = ca.nextInt();
						switch (claseavion) {
						case 1:
							cancelarec(mecnecmejnej, eccedula, claseavion);
							borrarnombreec(ecnombre, mecnecmejnej);
							borrarcedulaec(eccedula, mecnecmejnej);
							borrarlugarec(matrixec, mecnecmejnej);
							System.out.println("Su reserva ha sido cancelada");
							break;
						case 2:
							cancelarej(mecnecmejnej, ejcedula, claseavion);
							borrarnombrej(ejnombre, mecnecmejnej);
							borrarcedulaej(eccedula, mecnecmejnej);
							borrarlugarej(matrixej, mecnecmejnej);
							System.out.println("Su reserva ha sido cancelada");
							break;
						default:
							System.out.println("La opcion no es valida, por favor, intentelo de nuevo");
							break;
						}
					break;
					case 2:
						java.util.Scanner opc = new java.util.Scanner(System.in);
						int buscarced;
						System.out.println("Por favor, seleccione la clase en la que desea buscar");
						System.out.println("1. Economica" + "\t" + "2. Ejecutiva");
						buscarced = opc.nextInt();
						switch (buscarced) {
						case 1:
							busquedaporcedulaec(eccedula);
							break;
						case 2:
							busquedaporcedulaej(ejcedula);
							break;
						default:
							System.out.println("La opcion no es valida, intente de nuevo");
							break;
						}

					break;
					case 3:
						java.util.Scanner bp = new java.util.Scanner(System.in);
						int buscarpersona;
						System.out.println("Por favor, seleccione la clase en la que desea buscar");
						System.out.println("1. Economica" + "\t" + "2. Ejecutiva");
						buscarpersona = bp.nextInt();
						switch (buscarpersona) {
						case 1:
							buscarpersonasec(ecnombre);
							break;
						case 2:
							buscarpersonasej(ecnombre);
							break;
						default:
							System.out.println("La opcion no es valida, por favor, ingresela de nuevo");
							break;
						}

					break;
					case 4:
						sejoc(ejcedula);
						break;
					case 5:
						secoc(eccedula);
						break;
					case 6:
						secocven(eccedula);
						break;
					case 7:
						asientosejprint(matrixej);
						asientosecprint(matrixec);
						break;
					}
				break;	
				case 3:
					continuar =false;
					break;

				default:
					System.out.println("Su opción no fue válida, por favor, vuelva a intentarlo");
					break;
				}
				break;
			}
		
		}

	public static void asientosecprint(char[][] matrixec) {
		char[] vcp = { 'V', 'C', 'P' };
		System.out.println("[" + vcp[0] + "]" + "[" + vcp[1] + "]" + "[" + vcp[2] + "]" + "\t" + "[" + vcp[2] + "]"
				+ "[" + vcp[1] + "]" + "[" + vcp[0] + "]");

		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 6; j++) {
				System.out.print("[" + matrixec[i][j] + "]");
				if (j == 2) {
					System.out.print("\t");
				}

			}
			System.out.print("\n");
		}
	}

	public static void asientosejprint(char[][] matrixej) {
		char[] vcp = { 'V', 'P' };
		System.out.println("[" + vcp[0] + "]" + "[" + vcp[1] + "]" + "\t" + "\t" + "   " + "[" + vcp[1] + "]" + "["
				+ vcp[0] + "]");
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print("[" + matrixej[i][j] + "]");
				if (j == 1) {
					System.out.print("\t" + "\t" + "   ");
				}

			}
			System.out.print("\n");
		}
	}

	public static int ventanaec(int[][] eccedula, int posventana) {
		for (int i = 0; i < eccedula.length; i++) {
			for (int j = 0; j < 6; j++) {
				if (j == 0 || j == 5) {
					if (eccedula[i][j] == 0) {
						posventana = i;
						break;
					}
				}

			}
		}
		return posventana;
	}

	public static int centroec(int[][] eccedula, int poscentro) {
		for (int i = 0; i < eccedula.length; i++) {
			for (int j = 0; j < 6; j++) {
				if (j == 1 || j == 4) {
					if (eccedula[i][j] == 0) {
						poscentro = i;
						break;
					}
				}

			}
		}
		return poscentro;

	}

	public static int pasilloec(int[][] eccedula, int pospasillo) {
		for (int i = 0; i < eccedula.length; i++) {
			for (int j = 0; j < 6; j++) {
				if (j == 2 || j == 3) {
					if (eccedula[i][j] == 0) {
						pospasillo = i;
						break;
					}
				}

			}
		}
		return pospasillo;
	}

	public static int[][] cedulaec(int[][] eccedula, int posventana, int poscentro, int pospasillo, int j) {
		int cedula;
		try (java.util.Scanner ced = new java.util.Scanner(System.in)) {
			System.out.println("Por favor, ingrese el valor de la cedula");
			cedula = ced.nextInt();
			if (j == 0 || j == 5) {
				eccedula[posventana][j] = cedula;
			} else {
				if (j == 1 || j == 4) {
					eccedula[poscentro][j] = cedula;
				} else {
					if (j == 2 || j == 3) {
						eccedula[pospasillo][j] = cedula;
					}
				}
			}
			return eccedula;
		}
	}

	public static String[][] nombreec(String[][] ecnombre, int posventana, int pospasillo, int poscentro, int j) {
		String nombre;
			java.util.Scanner nom = new java.util.Scanner(System.in);
			System.out.println("Por favor, ingrese su nombre de la manera: Nombre_Apellido");
			nombre = nom.nextLine();
			if (j == 0 || j == 5) {
				ecnombre[posventana][j] = nombre;
			} else {
				if (j == 1 || j == 4) {
					ecnombre[poscentro][j] = nombre;
				} else {
					if (j == 2 || j == 3) {
						ecnombre[pospasillo][j] = nombre;
					}
				}
			}
			return ecnombre;
		}

	

	public static int ventanaej(int[][] ejcedula, int posventanaej) {
		for (int i = 0; i < ejcedula.length; i++) {
			for (int j = 0; j < 4; j++) {
				if (j == 0 || j == 3) {
					if (ejcedula[i][j] == 0) {
						posventanaej = i;
						break;
					}
				}

			}
		}
		return posventanaej;
	}

	public static int pasilloej(int[][] ejcedula, int pospasilloej) {
		for (int i = 0; i < ejcedula.length; i++) {
			for (int j = 0; j < 4; j++) {
				if (j == 1 || j == 2) {
					if (ejcedula[i][j] == 0) {
						pospasilloej = i;
						break;
					}
				}

			}
		}
		return pospasilloej;
	}

	public static int[][] cedulaej(int[][] ejcedula, int posventanaej, int pospasilloej, int k) {
		int cedula;
		try (java.util.Scanner ced = new java.util.Scanner(System.in)) {
			System.out.println("Por favor, ingrese el valor de la cedula");
			cedula = ced.nextInt();
			if (k == 0 || k == 3) {
				ejcedula[posventanaej][k] = cedula;
			} else {
				if (k == 1 || k == 2) {
					ejcedula[pospasilloej][k] = cedula;
				}
			}
			return ejcedula;
		}
	}

	public static String[][] nombreej(String[][] ejnombre, int posventanaej, int pospasillo, int k) {
		String nombre;
		java.util.Scanner nom = new java.util.Scanner(System.in);
			System.out.println("Por favor, ingrese su nombre de la manera: Nombre_Apellido");
			nombre = nom.nextLine();
			if (k == 0 || k == 5) {
				ejnombre[posventanaej][k] = nombre;
			} else {
				if (k == 2 || k == 3) {
					ejnombre[pospasillo][k] = nombre;
				}
			}
	return ejnombre;
	}

	public static char[][] consolaej(int[][] ejcedula, char[][] matrixej) {
		for (int i = 0; i < ejcedula.length; i++) {
			for (int j = 0; j < 4; j++) {
				if (ejcedula[i][j] != 0) {
					matrixej[i][j] = 'X';
				}
			}

		}
		return matrixej;
	}

	public static char[][] consolaec(int[][] eccedula, char[][] matrixec) {
		for (int i = 0; i < eccedula.length; i++) {
			for (int j = 0; j < 6; j++) {
				if (eccedula[i][j] != 0) {
					matrixec[i][j] = 'X';
				}
			}

		}
		return matrixec;
	}

	public static int[] cancelarec(int[] mecnecmejnej, int[][] eccedula, int claseavion) {
		try (java.util.Scanner cedcan = new java.util.Scanner(System.in)) {
			int cedulacan;
			System.out.println("Por favor, digite el numero de su cedula");
			cedulacan = cedcan.nextInt();
			for (int i = 0; i < eccedula.length; i++) {
				for (int j = 0; j < 6; j++) {
					if (eccedula[i][j] == cedulacan) {
						mecnecmejnej[0] = i;
						mecnecmejnej[1] = j;
						break;
					}
				}

			}
			return mecnecmejnej;
		}
	}

	public static int[] cancelarej(int[] mecnecmejnej, int[][] ejcedula, int claseavion) {
		try (java.util.Scanner cedcan = new java.util.Scanner(System.in)) {
			int cedulacan;
			System.out.println("Por favor, digite el numero de su cedula");
			cedulacan = cedcan.nextInt();
			for (int i = 0; i < ejcedula.length; i++) {
				for (int j = 0; j < 4; j++) {
					if (ejcedula[i][j] == cedulacan) {
						mecnecmejnej[2] = i;
						mecnecmejnej[3] = j;
					}
				}

			}
			return mecnecmejnej;
		}
	}

	public static char[][] borrarlugarec(char[][] matrixec, int[] mecnecmejnej) {
		int i, j;
		i = mecnecmejnej[0];
		j = mecnecmejnej[1];
		matrixec[i][j] = ' ';
		return matrixec;
	}

	public static int[][] borrarcedulaec(int[][] eccedula, int[] mecnecmejnej) {
		int i, j;
		i = mecnecmejnej[0];
		j = mecnecmejnej[1];
		eccedula[i][j] = 0;
		return eccedula;
	}

	public static String[][] borrarnombreec(String[][] ecnombre, int[] mecnecmejnej) {
		int i, j;
		i = mecnecmejnej[0];
		j = mecnecmejnej[1];
		ecnombre[i][j] = " ";
		return ecnombre;
	}

	public static char[][] borrarlugarej(char[][] matrixej, int[] mecnecmejnej) {
		int i, j;
		i = mecnecmejnej[2];
		j = mecnecmejnej[3];
		matrixej[i][j] = ' ';
		return matrixej;
	}

	public static int[][] borrarcedulaej(int[][] ejcedula, int[] mecnecmejnej) {
		int i, j;
		i = mecnecmejnej[2];
		j = mecnecmejnej[3];
		ejcedula[i][j] = 0;
		return ejcedula;
	}

	public static String[][] borrarnombrej(String[][] ejnombre, int[] mecnecmejnej) {
		int i, j;
		i = mecnecmejnej[2];
		j = mecnecmejnej[3];
		ejnombre[i][j] = " ";
		return ejnombre;
	}

	public static void busquedaporcedulaec(int[][] eccedula) {
		try (java.util.Scanner cedbus = new java.util.Scanner(System.in)) {
			int cedulabus;
			System.out.println("Por favor, digite el numero de su cedula");
			cedulabus = cedbus.nextInt();
			for (int i = 0; i < eccedula.length; i++) {
				for (int j = 0; j < 6; j++) {
					if (eccedula[i][j] == cedulabus) {
						if (j == 0 || j == 5) {
							if (j == 0) {
								System.out.println("El puesto se encuentra en la ventana izquierda, en la fila " + i);
							}
							if (j == 5) {
								System.out.println("El puesto se encuentra en la ventana derecha, en la fila " + i);
							}
						}
						if (j == 1 || j == 4) {
							if (j == 1) {
								System.out.println("El puesto se encuentra en el centro izquierdo, en la fila " + i);
							}
							if (j == 4) {
								System.out.println("El puesto se encuentra en el centro derecho, en la fila " + i);
							}
						}
						if (j == 2 || j == 3) {
							if (j == 2) {
								System.out.println("El puesto se encuentra en el pasillo izquierdo, en la fila " + i);
							}
							if (j == 3) {
								System.out.println("El puesto se encuentra en el pasillo derecho, en la fila " + i);
							}
						}
					} else {
						System.out.println("La cedula no puede ser encontrada");
					}
				}
			}
		}
	}

	public static void busquedaporcedulaej(int[][] ejcedula) {
		try (java.util.Scanner cedbus = new java.util.Scanner(System.in)) {
			int cedulabus;
			System.out.println("Por favor, digite el numero de su cedula");
			cedulabus = cedbus.nextInt();
			for (int i = 0; i < ejcedula.length; i++) {
				for (int j = 0; j < 4; j++) {
					if (ejcedula[i][j] == cedulabus) {
						if (j == 0 || j == 3) {
							if (j == 0) {
								System.out.println("El puesto se encuentra en la ventana izquierda, en la fila " + i);
							}
							if (j == 3) {
								System.out.println("El puesto se encuentra en la ventana derecha, en la fila " + i);
							}
						}
						if (j == 1 || j == 2) {
							if (j == 1) {
								System.out.println("El puesto se encuentra en el pasillo izquierdo, en la fila " + i);
							}
							if (j == 2) {
								System.out.println("El puesto se encuentra en el pasillo derecho, en la fila " + i);
							}
						}
					} else {
						System.out.println("La cedula no puede ser encontrada");
					}
				}
			}
		}
	}

	public static void busquedapornombreec(String[][] ecnombre) {
		try (java.util.Scanner nombus = new java.util.Scanner(System.in)) {
			String nombrebus;
			System.out.println("Por favor, escriba su nombre de la manera Nombre_Apellido");
			nombrebus = nombus.next();
			for (int i = 0; i < ecnombre.length; i++) {
				for (int j = 0; j < 4; j++) {
					if (ecnombre[i][j]==nombrebus) {
						if (j == 0 || j == 5) {
							if (j == 0) {
								System.out.println("El puesto se encuentra en la ventana izquierda, en la fila " + i);
							}
							if (j == 5) {
								System.out.println("El puesto se encuentra en la ventana derecha, en la fila " + i);
							}
						}
						if (j == 1 || j == 4) {
							if (j == 1) {
								System.out.println("El puesto se encuentra en el centro izquierdo, en la fila " + i);
							}
							if (j == 4) {
								System.out.println("El puesto se encuentra en el centro derecho, en la fila " + i);
							}
						}
						if (j == 2 || j == 3) {
							if (j == 2) {
								System.out.println("El puesto se encuentra en el pasillo izquierdo, en la fila " + i);
							}
							if (j == 3) {
								System.out.println("El puesto se encuentra en el pasillo derecho, en la fila " + i);
							}
						}
					} else {
						System.out.println("El nombre no puede ser encontrado");
					}
				}
			}
		}
	}

	public static void busquedapornombreej(String[][] ejnombre) {
		try (java.util.Scanner nombus = new java.util.Scanner(System.in)) {
			String nombrebus;
			System.out.println("Por favor, escriba el nombre de la manera: Nombre_Apellido");
			nombrebus = nombus.next();
			for (int i = 0; i < ejnombre.length; i++) {
				for (int j = 0; j < 4; j++) {
					if (ejnombre[i][j] == nombrebus) {
						if (j == 0 || j == 3) {
							if (j == 0) {
								System.out.println("El puesto se encuentra en la ventana izquierda, en la fila " + i);
							}
							if (j == 3) {
								System.out.println("El puesto se encuentra en la ventana derecha, en la fila " + i);
							}
						}
						if (j == 1 || j == 2) {
							if (j == 1) {
								System.out.println("El puesto se encuentra en el pasillo izquierdo, en la fila " + i);
							}
							if (j == 2) {
								System.out.println("El puesto se encuentra en el pasillo derecho, en la fila " + i);
							}
						}
					} else {
						System.out.println("El nombre no puede ser encontrado");
					}
				}
			}
		}
	}

	public static void buscarpersonasej(String[][] ejnombre) {
		boolean persona = false;
		for (int i = 0; i < ejnombre.length; i++) {
			for (int j = 0; j < ejnombre[i].length; j++) {
				for (int k = 0; k < ejnombre.length; k++) {
					for (int l = 0; l < ejnombre[i].length; l++) {
						if (ejnombre[i][j].equals(ejnombre[k][l]) && (i != k || j != l)) {
							persona = true;
						}
					}
				}
			}
		}
		if (persona = true) {
			System.out.println("Si existe otra persona con el mismo nombre en la clase");
		} else {
			System.out.println("No existe otra persona con el mismo nombre en la clase");
		}
	}

	public static void buscarpersonasec(String[][] ecnombre) {
		boolean persona = false;
		for (int i = 0; i < ecnombre.length; i++) {
			for (int j = 0; j < ecnombre[i].length; j++) {
				for (int k = 0; k < ecnombre.length; k++) {
					for (int l = 0; l < ecnombre[i].length; l++) {
						if (ecnombre[i][j].equals(ecnombre[k][l]) && (i != k || j != l)) {
							persona = true;
						}
					}
				}
			}
		}
		if (persona = true) {
			System.out.println("Si existe otra persona con el mismo nombre en la clase");
		} else {
			System.out.println("No existe otra persona con el mismo nombre en la clase");
		}
	}

	public static void sejoc(int[][] ejcedula) {
		int countsejoc = 0;
		for (int i = 0; i < ejcedula.length; i++) {
			for (int j = 0; j < ejcedula[i].length; j++) {
				if (ejcedula[i][j] != 0) {
					countsejoc++;
				}
			}
		}
		System.out.println("El numero de sillas ocupadas en la clase ejecutiva es/son:" + countsejoc);
	}

	public static void secoc(int[][] eccedula) {
		int countsecoc = 0;
		for (int i = 0; i < eccedula.length; i++) {
			for (int j = 0; j < eccedula[i].length; j++) {
				if (eccedula[i][j] == 0) {
					countsecoc++;
				}
			}
		}
		System.out.println("El numero de sillas disponibles en la clase economica es/son:" + countsecoc);
	}

	public static void secocven(int[][] eccedula) {
		int countsecocven = 0;
		for (int i = 0; i < eccedula.length; i++) {
			for (int j = 0; j < eccedula[i].length; j++)
				if (j == 0 || j == 5) {
					if (eccedula[i][j] == 0) {
						countsecocven++;
					}
				}
		}

		System.out.println("El numero de sillas disponibles en la clase economica es/son:" + countsecocven);
	}


}


